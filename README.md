# somatic variant calling pipeline template

Template for somatic variant calling on an SGE system.

To run this you need to edit the config to give appropriate values for project
specific fields. You will likely also need to update the modules loaded by
config.sh. See the example\_config\_files folder for an example config file and
example files for PAIRS and TUMOUR\_NORMAL\_PAIRS.

The PAIRS file should comprise tab-separated values with the following format
for each FASTQ pair to be analyzed:

    SAMPLE	LIBRARY	READ_GROUP_ID	FQ1	FQ2

The read group tag should be used to distinguish different runs, libraries and
samples and will be used as the @RG tag in BAMs generated from the FASTQ pair.

The TUMOUR\_NORMAL\_PAIRS file should one line per tumour-normal pair to be
analyzed. The same normal sample may appear more than once (e.g. if analyzing
two tumours from the same individual) but each tumour ID should be unique. It
should comprise tab-separate values in the format:

    TUMOUR_ID NORMAL_ID	COMPARISON_ID

## Running

Make sure you run from the directory in which you want your output files to be
created.

    git clone git@git.ecdf.ed.ac.uk:dp_somatic_pipeline/somatic_variant_calling.git
    vim somatic_variant_calling/config.sh # substitute `vim` with an editor of 
                                          # your choice and edit the config 
                                          # as per your requirements
    ./somatic_variant_calling/run.sh

The final command should submit the required jobs to the scheduler.   
