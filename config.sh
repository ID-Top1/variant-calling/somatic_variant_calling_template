#!/bin/bash
# Configure modules
. /etc/profile.d/modules.sh
# Load modules
module load igmm/apps/samtools/1.10
module load igmm/apps/bwa/0.7.16
module load R/3.5.3
module load python/2.7.10
module load igmm/apps/bcftools/1.10.2

########## PROJECT SPECIFIC FIELDS ########## 
export PAIRS= # tab-separated fields: SAMPLE LIBRARY RUN FQ1 FQ2
export TUMOUR_NORMAL_PAIRS= # tab-separated fields: TUMOUR NORMAL SAMPLE
export REF= # reference fasta - expect bwa indexes, .dict and .fai files
export READ_LENGTH= # FQ read length
export KNOWN_SITES= # for BQSR and picard metrics
export KNOWN_INDELS= # for SVABA
export KNOWN_SVS= # for SVABA
export PROBLEM_REGIONS= # for PINDEL - avoid telomore/centromere regions
export GERMLINE_AF_VCF= # for MUTECT2
export COMMON_GERMLINE_VCF= # for GetPileupSummaries part of MUTECT2 filtering
# IF R libraries required by GATK are in a non-standard location uncomment and
# edit the line below as required
#export R_LIBS=/path/to/custom/RLIBRARY:$R_LIBS 

########## DEFAULT VALUES ##########
export GATK_VERSION=4.1.9.0
export GATK=./tools/gatk-${GATK_VERSION}/gatk
export STRELKA_VERSION=2.9.10
export STRELKA_DIR=tools/strelka-${STRELKA_VERSION}.centos6_x86_64
export MANTA_VERSION=1.6.0
export MANTA_DIR=tools/manta-${MANTA_VERSION}.centos6_x86_64
export SVABA=./tools/svaba/bin/svaba
export PINDEL=./tools/pindel/pindel
export PLATYPUS=./tools/Platypus/Platypus.py
export SOMALIER=./tools/somalier
export CHROM_BED=./ref/main_contigs.bed
export CHROM_INTERVALS=ref/main_contigs.interval_list
