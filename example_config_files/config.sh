#!/bin/bash
# Configure modules
. /etc/profile.d/modules.sh
# Load modules
module load igmm/apps/samtools/1.10
module load igmm/apps/bwa/0.7.16
module load R/3.5.3
module load python/2.7.10
module load igmm/apps/bcftools/1.10.2

########## PROJECT SPECIFIC FIELDS ########## 
export PAIRS=pairs.txt # tab-separated fields: SAMPLE LIBRARY RUN FQ1 FQ2
export TUMOUR_NORMAL_PAIRS=tumour_normals.txt # tab-separated fields: TUMOUR NORMAL SAMPLE
export REF=/exports/igmm/software/pkg/el7/apps/bcbio/share2/genomes/Hsapiens/hg38/bwa/hg38.fa # reference fasta - expect bwa indexes, .dict and .fai files
export READ_LENGTH=150 # FQ read length
export KNOWN_SITES=/exports/igmm/eddie/IGMM-VariantAnalysis/resources/dbSNP151.hg38.vcf.gz # for BQSR and picard metrics
export KNOWN_INDELS=ref/dbSNP151.hg38.indels.vcf.gz # for SVABA
export KNOWN_SVS=ref/dbvar_common_combined.bed # for SVABA
export PROBLEM_REGIONS=ref/centromeres.bed # for PINDEL - avoid telomere/centromere regions
export GERMLINE_AF_VCF=ref/af-only-gnomad.hg38.vcf.gz # for MUTECT2
export COMMON_GERMLINE_VCF=ref/small_exac_common_3.hg38.vcf.gz # for GetPileupSummaries part of MUTECT2 filtering
export R_LIBS=/gpfs/igmmfs01/eddie/ajackson-lab/dp_eddie_libs/Rlibs/x86_64-pc-linux-gnu-library/3.5:$R_LIBS # if R libraries required by GATK are in a non-standard location

########## DEFAULT VALUES ##########
export GATK_VERSION=4.1.9.0
export GATK=./tools/gatk-${GATK_VERSION}/gatk
export STRELKA_VERSION=2.9.10
export STRELKA_DIR=tools/strelka-${STRELKA_VERSION}.centos6_x86_64
export MANTA_VERSION=1.6.0
export MANTA_DIR=tools/manta-${MANTA_VERSION}.centos6_x86_64
export SVABA=./tools/svaba/bin/svaba
export PINDEL=./tools/pindel/pindel
export PLATYPUS=./tools/Platypus/Platypus.py
export SOMALIER=./tools/somalier
export CHROM_BED=./ref/main_contigs.bed
export CHROM_INTERVALS=ref/main_contigs.interval_list
