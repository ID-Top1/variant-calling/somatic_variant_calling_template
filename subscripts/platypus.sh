#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=48:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 4

config=$1
source $config
set -exou pipefail
out_dir=platypus_output
vcf_out=${out_dir}/all_samples.platypus.vcf
norm_out=${out_dir}/all_samples.platypus.sorted.norm.vcf.gz
bam_list=${out_dir}/bams.txt

mkdir -p $out_dir
ls alignments/merged/*bqsr.bam > $bam_list

echo $(date) Running Platypus for all samples
python $PLATYPUS callVariants \
    --nCPU=$NSLOTS \
    --logFileName=${out_dir}/log.txt \
    --refFile=$REF \
    --bamFiles=$bam_list \
    --output=${vcf_out}

echo $(date) Compressing, sorting, normalizing and indexing output
bgzip ${vcf_out}
bcftools reheader -f $REF.fai ${vcf_out}.gz | \
    bcftools sort -O u | \
    bcftools norm -f $REF -O z -o $norm_out
tabix -p vcf ${norm_out}

echo $(date) Done
echo $?
