#!/bin/bash
#$ -V
#$ -cwd
#$ -l h_rt=1:00:00
#$ -l h_vmem=1G

if [ $# -ne 1 ]
then
    echo "Usage: $0 alignment_directory"
    exit 1
fi

search_dir=$1

for bai in `find $search_dir -name '*.bai'`
do
    bam1=$(echo $bai | sed s/\.bai$//)
    bam2=$(echo $bai | sed s/\.bai$/.bam/)
    if [ -e "$bam1" ]
    then
        echo $bam1 exists - keeping $bai
    elif [ -e "$bam2" ]
    then
        echo $bam2 exists - keeping $bai
    else
        echo removing $bai
        rm $bai
    fi
done
