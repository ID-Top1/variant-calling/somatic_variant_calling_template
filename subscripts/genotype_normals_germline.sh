#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G
#$ -l h_rt=72:00:00

config=$1
id_list=$2
source $config
set -exou pipefail
sample_id=$(awk "NR == $SGE_TASK_ID" $id_list)
normal_bam=alignments/merged/${sample_id}.mkdups.bqsr.bam
mkdir -p mutect2_calls/normal_gvcfs
gvcf=mutect2_calls/normal_gvcfs/$(basename $normal_bam .mkdups.bqsr.bam).g.vcf.gz

echo $(date) Creating GVCF for $sample_id
$GATK --java-options "-Xmx8G" HaplotypeCaller \
    -R $REF \
    -I $normal_bam \
    -O $gvcf \
    -ERC GVCF \
    -G StandardAnnotation \
    -G AS_StandardAnnotation 

echo $(date) Done - GVCF written to $gvcf
echo $?

