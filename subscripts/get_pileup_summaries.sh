#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G

config=$1
id_list=$2
source $config

set -exou pipefail
sample_id=$(awk "NR == $SGE_TASK_ID" $id_list)
bam=alignments/merged/${sample_id}.mkdups.bqsr.bam
pileup_dir=mutect2_calls/pileup_summaries
mkdir -p $pileup_dir
echo $(date) Getting pileup summaries for $sample_id
$GATK --java-options "-Xmx8G" GetPileupSummaries \
    -R $REF \
    -I $bam \
    -V $COMMON_GERMLINE_VCF \
    -L $COMMON_GERMLINE_VCF \
    -O ${pileup_dir}/${sample_id}.pileupsummaries.table

echo $(date) Done 
echo $?
