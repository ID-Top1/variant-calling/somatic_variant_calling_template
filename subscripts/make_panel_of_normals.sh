#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=8G

config=$1
source $config

set -exou pipefail
mkdir -p mutect2_calls/normal_variant_calls/genomics-db
gene_db=mutect2_calls/normal_variant_calls/genomics-db/${SGE_TASK_ID}

interval=$(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | \
    perl -wane 'print "$F[0]:1-$F[1]\n";' | \
    awk "NR == $SGE_TASK_ID")
vcf_string=''
for vcf in mutect2_calls/normal_vcfs/*.vcf.gz
do
    vcf_string="$vcf_string -V $vcf"
done 

$GATK --java-options "-Xmx4G" GenomicsDBImport \
    -R $REF \
    -L $interval \
    --genomicsdb-workspace-path $gene_db \
    $vcf_string

outdir=mutect2_calls/panel_of_normals/split_vcfs
mkdir -p $outdir
vcf=${outdir}/var.normal_panel.${SGE_TASK_ID}.raw.vcf.gz
echo $(date) Genotyping for $interval
$GATK --java-options "-Xmx4G" CreateSomaticPanelOfNormals \
    --germline-resource $GERMLINE_AF_VCF \
    -L $interval \
    -V gendb://${gene_db} \
    -O ${vcf}
    
echo $(date) Done
echo $?
