#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=24:00:00
#$ -l h_vmem=4G
# Configure modules
. /etc/profile.d/modules.sh

config=$1
source ${config}

#set -eou pipefail
mkdir -p tools ref

gatk_dir=tools/gatk-${GATK_VERSION}
if [ -d $gatk_dir ]
then
    echo $gatk_dir already exists, nothing to do
else
    cd tools
    echo $(date) Getting GATK v${GATK_VERSION}
    wget https://github.com/broadinstitute/gatk/releases/download/${GATK_VERSION}/gatk-${GATK_VERSION}.zip
    unzip -o gatk-${GATK_VERSION}.zip
    rm gatk-${GATK_VERSION}.zip
    cd ..
fi

if [ -x tools/goleft_linux64 ]
then
    echo goleft already present, nothing to do
else
    cd tools
    wget https://github.com/brentp/goleft/releases/download/v0.2.4/goleft_linux64
    chmod +x goleft_linux64
    cd ../
fi

if [ -d $STRELKA_DIR ]
then
    echo $STRELKA_DIR already exists, nothing to do
else
    cd tools
    echo $(date) Getting Strelka v${STRELKA_VERSION}
    wget https://github.com/Illumina/strelka/releases/download/v${STRELKA_VERSION}/strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    tar xvf strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    rm strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    cd ..
fi


if [ -d $MANTA_DIR ]
then
    echo $MANTA_DIR already exists, nothing to do
else
    cd tools
    echo $(date) Getting Manta v${MANTA_VERSION}
    wget https://github.com/Illumina/manta/releases/download/v${MANTA_VERSION}/manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    tar xvf manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    rm manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    cd ..
fi

if [ -x $SVABA ]
then
    echo $SVABA already exists, nothing to do
else
    cd tools
    echo $(date) Getting svaba
    git clone --recursive https://github.com/walaj/svaba
    cd svaba
    ./configure
    make
    make install
    cd ../../
fi

if [ -x $PLATYPUS ]
then
    echo $PLATYPUS already exists, nothing to do
else
    cd tools
    echo $(date) Getting Platypus
    wget https://www.rdm.ox.ac.uk/files/research/lunter-group/platypus-latest.tgz
    tar xvf platypus-latest.tgz
    rm platypus-latest.tgz
    mv Platypus_*/ Platypus
    cd Platypus
    echo $(date) Building Platypus
    ./buildPlatypus.sh
    cd ../../
fi

if [ -x $SOMALIER ]
then
    echo $SOMALIER already exists, nothing to do
else
    cd tools
    echo $(date) Getting somalier
    wget https://github.com/brentp/somalier/releases/download/v0.2.12/somalier
    chmod +x somalier
    cd ../
fi

echo $(date) Creating main chromosome intervals
grep -wP '^(chr)?[0-9XY]+' ${REF}.fai | \
    perl -wane 'print "$F[0]\t0\t$F[1]\n";' > $CHROM_BED

$GATK  --java-options "-Xmx1G" BedToIntervalList \
    -I $CHROM_BED \
    -O $CHROM_INTERVALS \
    -SD ${REF}.dict

echo $(date) Done
echo $?
