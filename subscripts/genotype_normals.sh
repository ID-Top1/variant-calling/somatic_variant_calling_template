#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G
#$ -l h_rt=72:00:00
config=$1
id_list=$2
source $config
set -exou pipefail
sample_id=$(awk "NR == $SGE_TASK_ID" $id_list)
normal_bam=alignments/merged/${sample_id}.mkdups.bqsr.bam
mkdir -p mutect2_calls/normal_sample_vcfs
vcf=mutect2_calls/normal_sample_vcfs/$(basename $normal_bam .mkdups.bqsr.bam).vcf.gz

echo $(date) Creating GVCF for $sample_id
$GATK --java-options "-Xmx8G" Mutect2 \
    -R $REF \
    -I $normal_bam \
    --max-mnp-distance 0 \
    -O $vcf

echo $(date) Done - VCF written to $vcf
echo $?
