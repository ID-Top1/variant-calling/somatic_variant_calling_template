#!/bin/bash

source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
script_dir=${source_dir}/subscripts
config=${source_dir}/config.sh
source $config

set -exou pipefail

mkdir -p logs tmp

# might have duplicate normal samples for diff tumours - get unique ID lists
cut -f 1 $PAIRS | sort | uniq > all_samples_unique.txt
cut -f 2 $TUMOUR_NORMAL_PAIRS | sort | uniq > normal_samples_unique.txt

# array tasks depend on number of unique/normal/tumour samples and standard contigs
fq_tasks=$(cat $PAIRS | wc -l)
sample_tasks=$(cat all_samples_unique.txt | wc -l)
normals_tasks=$(cat normal_samples_unique.txt | wc -l)
tumour_tasks=$(cat $TUMOUR_NORMAL_PAIRS  | wc -l )
chroms=( $(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | cut -f 1) )
gt_tasks=${#chroms[@]}


# SETUP required tools
hold=$(qsub -terse ${script_dir}/setup.sh $config | cut -f 1 -d.)

# FASTQ to processed alignments
hold=$(qsub -terse -hold_jid ${hold} -t 1-${fq_tasks} ${script_dir}/fq2ubam.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} -t 1-${fq_tasks} ${script_dir}/align.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/process_alignments.sh \
    $config \
    all_samples_unique.txt | \
    cut -f 1 -d. )

# clean up orphan bam indexes
qsub -hold_jid ${hold} ${script_dir}/clean_orphan_bais.sh alignments

# misc. QC
metrics_hold=$(qsub -terse -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/multi_metrics.sh $config all_samples_unique.txt | cut -f 1 -d .)
qsub -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/wgs_metrics.sh $config all_samples_unique.txt
qsub -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/oxog_metrics.sh $config all_samples_unique.txt
qsub -hold_jid ${hold} -t 1-${fq_tasks} ${script_dir}/fastqc.sh

# Somatic genotyping with strelka 
qsub -hold_jid ${hold} -t 1-${tumour_tasks} ${script_dir}/manta_and_strelka.sh $config

# Joint genotyping with platypus
qsub -hold_jid ${hold} ${script_dir}/platypus.sh $config

# Indel/SV genotyping with svaba
qsub -hold_jid ${hold} -t 1-${tumour_tasks} ${script_dir}/svaba.sh $config

# Genotyping with GATK/Mutect2
contam_hold=$(qsub -terse -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/get_pileup_summaries.sh $config all_samples_unique.txt | cut -f 1 -d .)
germline_hold=$(qsub -terse -hold_jid ${hold} -t 1-${normals_tasks} ${script_dir}/genotype_normals_germline.sh $config normal_samples_unique.txt | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} -t 1-${gt_tasks} ${script_dir}/import_gvcfs.sh $config | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} -t 1-${gt_tasks} ${script_dir}/joint_genotype_germline.sh $config | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} ${script_dir}/concat_and_filter_germline_vcfs.sh $config | cut -f 1 -d.)
somalier_hold=$(qsub -terse -hold_jid ${germline_hold} -t 1-${sample_tasks} ${script_dir}/somalier_extract.sh $config | cut -f 1 -d .)
qsub -hold_jid ${somalier_hold} ${script_dir}/somalier_relate.sh $config

hold=$(qsub -terse -hold_jid ${hold} -t 1-${normals_tasks} ${script_dir}/genotype_normals.sh $config normal_samples_unique.txt | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} -t 1-${gt_tasks} ${script_dir}/make_panel_of_normals.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} ${script_dir}/concat_normal_vcfs.sh $config)
# Mutect2 with panel of normals and calculating contamination using germline genotypes
qsub -hold_jid ${hold},${contam_hold},${germline_hold} -l h_rt=96:00:00 -t 1-${tumour_tasks} ${script_dir}/mutect2.sh $config
